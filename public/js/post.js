function User() {
    var user_email = '';
    var user_name;
    var user_photo;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            user_name = user.displayName;
            user_photo = user.photoURL;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById("logout-btn");
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function () {
                    alert('success');
                    window.location.href = "signin.html"
                }).catch(
                    function () {
                        alert('sign out failed');
                    }
                )

            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post').innerHTML = "";
            document.getElementById('comment_list').innerHTML = "";
        }
    });
}

function WritePost(key) {
    var post = document.getElementById('post');
    var postRef = firebase.database().ref('com_list/' + key);
    postRef.once('value').then(function (snapshot) {
        var title = snapshot.val().title;
        var article = snapshot.val().data;
        var author = snapshot.val().displayName;
        var author_pic = snapshot.val().photo;

        var pre = "<div class='my-3 p-3 bg-white rounded '><h5 style='text-align:center;' class='border-bottom border-gray pb-2 mb-0'><strong>" + title + "</strong></h5>";
        var AuthorPart = "<div class='row px-3'><div class='col-bg-1'><strong class='row '>" + author + "</strong><img src=' " + author_pic + " ' alt='大頭貼' class='row mr-2  rounded' style='height:50px; width:50px;'></div>";
        var words = "<div class='col-sm-10 ml-3'style='font-size:1rem;'>" + article + "</div>";
        var ending = "</div>";
        post.innerHTML = pre + AuthorPart + words + ending ;
    });
}
function notifyMe(title, reply, url) {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification(title, {
            icon: 'img/cr_logo.png',
            body: reply
        });

        notification.onclick = function () {
            window.open(url);
        };

    }


}
function leaveComment(key) {
    var post_btn = document.getElementById('post_btn');
    var comment = document.getElementById('comment');
    post_btn.addEventListener('click', function () {
        if (comment.value != "") {
            var postRef = firebase.database().ref('com_list/' + key);
            var commentsRef = firebase.database().ref('com_list/' + key + '/comments');
            var newCommentRef = commentsRef.push();
            var d = new Date();
            var user = firebase.auth().currentUser;
            newCommentRef.set({
                data: comment.value,
                person: user.displayName,
                photo: user.photoURL,
                date: d.getTime()
            });
            console.log(comment.value);
            postRef.once('value').then(function (snapshot) {
                var title = snapshot.val().title;
                var article = snapshot.val().data;
                var author = snapshot.val().displayName;
                notifyMe(user.displayName +"回應了"+ author+"的貼文",comment.value,"post.html?"+key);
            });
            comment.value = "";
            //alert('comment success');
        }
    });
}

function showComment(key) {
    var commentsRef = firebase.database().ref('com_list/' + key + '/comments');
    var total_Comment = [];
    var first_count = 0;
    var second_count = 0;

    commentsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                var AuthorPart = "<div class='row  bg-white'><div class='col-sm-2'><strong class='row ml-3'>" + childData.person + "</strong><img src=' " + childData.photo + " ' alt='大頭貼' class='row ml-3' style='width:50px;height:50px;'></div>";
                var leave_time = function () {
                    let cur_time = new Date();
                    var diff = parseInt((cur_time - childData.date) / 1000); //seconds
                    if (0<= diff && diff < 60)// in a minute
                    {
                        return "Just now";
                    } else if (60 < diff && diff < 3600) {//between 0~60 min
                        let show = (diff / 60).toString();
                        if (show.split('.')[0] == "1") {
                            return "A minute ago";
                        }
                            return show.split('.')[0] + " minutes ago";
                    } else if (3600 < diff && diff < 216000) { //between 1~24 hr
                        let show = (diff / 3600).toString();
                        if (show.split('.')[0] == "1") {
                            return "An hour ago";
                        }
                        //alert(diff);
                        return show.split('.')[0] + " hours ago";
                    } else if (3600*60 < diff && diff < 3600*60*24) {
                        let show = (diff / 3600 / 24).toString();
                        if (show.split('.')[0] == "1") {
                            return "A day ago";
                        }
                        return show.split('.')[0] + " days ago";
                    } else if (3600*60*24 < diff && diff < 3600*60*24*30) {
                        let show = (diff / 3600 / 24 / 30).toString();
                        if (show.split('.')[0] == "1") {
                            return "A month ago";
                        }
                        return show.split('.')[0] + " months ago";
                    } else if (3600 *60* 24 * 30 * 12 < diff) {
                        let show = (diff / 3600 / 24 / 30 / 12).toString();
                        if (show.split('.')[0] == "1") {
                            return "An year ago";
                        }
                        return show.split('.')[0] + " years ago";
                    } else
                        return "no data";
                };
                var words = "<div class='col-sm-10 '><div class='row'><p class='col-sm-9' style='font-size:1rem;'>" + childData.data + "</p>";
                var commentTime = "<p class='col-sm-3' style='font-size:0.2rem; color:blue;'>" + leave_time() + "</p></div></div>";
                var ending = "</div>";
                total_Comment[total_Comment.length] = AuthorPart + words + commentTime + ending + "<hr>";
                first_count += 1;
            });
            document.getElementById("comment_list").innerHTML = total_Comment.join('');

            commentsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var AuthorPart = "<div class='row  bg-white'><div class='col-2'><strong class='row ml-3'>" + childData.person + "</strong><img src=' " + childData.photo + " ' alt='大頭貼' class='row ml-3' style='width:40px;height:40px;'></div>";
                    var leave_time = function () {
                        let cur_time = new Date();
                        var diff = parseInt((cur_time - childData.date) / 1000); //seconds
                        if (0<= diff && diff < 60)// in a minute
                        {
                            return "Just now";
                        } else if (60 < diff && diff < 3600) {//between 0~60 min
                            let show = (diff / 60).toString();
                            if (show.split('.')[0] == "1") {
                                return "A minute ago";
                            }
                                return show.split('.')[0] + " minutes ago";
                        } else if (3600 < diff && diff < 216000) { //between 1~24 hr
                            let show = (diff / 3600).toString();
                            if (show.split('.')[0] == "1") {
                                return "An hour ago";
                            }
                            //alert(diff);
                            return show.split('.')[0] + " hours ago";
                        } else if (3600*60 < diff && diff < 3600*60*24) {
                            let show = (diff / 3600 / 24).toString();
                            if (show.split('.')[0] == "1") {
                                return "A day ago";
                            }
                            return show.split('.')[0] + " days ago";
                        } else if (3600*60*24 < diff && diff < 3600*60*24*30) {
                            let show = (diff / 3600 / 24 / 30).toString();
                            if (show.split('.')[0] == "1") {
                                return "A month ago";
                            }
                            return show.split('.')[0] + " months ago";
                        } else if (3600 *60* 24 * 30 * 12 < diff) {
                            let show = (diff / 3600 / 24 / 30 / 12).toString();
                            if (show.split('.')[0] == "1") {
                                return "An year ago";
                            }
                            return show.split('.')[0] + " years ago";
                        } else
                            return "no data";
                    };
                    var words = "<div class='col-10 '><div class='row'><p class='col-sm-9' style='font-size:1rem;'>" + childData.data + "</p>";
                    var commentTime = "<p class='col-sm-3' style='font-size:0.2rem; color:blue;'>" + leave_time() + "</p></div></div>";
                    var ending = "</div>";
                    total_Comment[total_Comment.length] = AuthorPart + words + commentTime + ending+ "<hr>";
                    document.getElementById("comment_list").innerHTML = total_Comment.join('');
                }
            })
        });
};
window.onload = function () {
    var fkey = window.location.href.split('?')[1];
    var key = fkey.split('#')[0];
    User();
    //alert(key);
    WritePost(key);
    showComment(key);
    leaveComment(key);
};