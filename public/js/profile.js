firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        //
        //alert('2');
        var menu = document.getElementById('dynamic-menu');
        menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
        var btnLogout = document.getElementById("logout-btn");
        btnLogout.addEventListener('click', e => {
            firebase.auth().signOut().then(function () {
                alert('success');
                window.location.href = "signin.html"
            }).catch(
                function () {
                    alert('sign out failed');
                }
            );
        });
        //upload user info - name
        var Cname = document.getElementById("Chellenger-name");
        var name = document.getElementById("name");

        if (user.displayName) {
            Cname.style.display = "inline";
            Cname.innerText = user.displayName;
        } else {
            name.style.display = "inline";
            name.placeholder = "Enter your name";
            name.addEventListener('input', function writeData() {
                //write name into firebase's database
                /*fireabse.database().ref('users/'+ user.uid).set({

                });*/
                user.updateProfile({
                    displayName: name.value
                }).then(function () {
                    console.log('更名成功');
                }), function (error) {
                    console.log('更名失敗:' + error);
                };
            });
        }
        //upload user info - trophy
        var Ctrophy = document.getElementById("Chellenger-trophy");
        var trophy = document.getElementById("trophy");
        var trophyRef = firebase.database().ref('users/' + user.uid + '/trophy');
        trophyRef.once('value')
            .then(function (snapshot) {
                let trophyExist = snapshot.exists();
                if (trophyExist) {
                    Ctrophy.style.display = "inline";
                    trophyRef.on('value', function (snapshot) {
                        Ctrophy.innerText = snapshot.val();
                    });
                } else {
                    trophy.style.display = "inline";
                    trophy.placeholder = "Enter your trophy numbers";
                    trophy.addEventListener('input', function writeData() {
                        //write name into firebase's database
                        firebase.database().ref('users/' + user.uid).update({
                            trophy: trophy.value
                        }).then(function () {
                            console.log('獎盃數已登錄');
                        });;
                    });
                }
            });

        //upload user info - level
        var Clevel = document.getElementById("Chellenger-level");
        var level = document.getElementById("level");
        var levelRef = firebase.database().ref('users/' + user.uid + '/level');
        levelRef.once('value')
            .then(function (snapshot) {
                let levelExist = snapshot.exists();
                if (levelExist) {
                    Clevel.style.display = "inline";
                    levelRef.on('value', function (snapshot) {
                        Clevel.innerText = snapshot.val();
                    });
                } else {
                    level.style.display = "inline";
                    level.placeholder = "Enter your level";
                    level.addEventListener('input', function writeData() {
                        //write name into firebase's database
                        firebase.database().ref('users/' + user.uid).update({
                            level: level.value,
                        }).then(function () {
                            console.log('等級已登錄');
                        });;
                    });
                }
            });
        // show user's photo
        var output = document.getElementById('output');
        if (user.photoURL) {
            output.src = user.photoURL;
        } else {
            output.src = "img/JtCRhuU.gif";
        }
    } else {
        alert('please sign in !');
        window.location.href = "signin.html";
    }
});
// change button
// when can't get element , go check out parse sequence of html and javascript file.
var changeProfile = document.getElementById('change');
var changing = false;
changeProfile.addEventListener('click', function () {
    changing = !changing;
    let name = document.getElementById("name");
    let trophy = document.getElementById("trophy");
    let level = document.getElementById("level");
    if (changing) {
        name.style.display = "inline";
        name.placeholder = "Enter your name";
        trophy.style.display = "inline";
        trophy.placeholder = "Enter your trophy numbers";
        level.style.display = "inline";
        level.placeholder = "Enter your level";

        var user = firebase.auth().currentUser;
        name.addEventListener('input', function writeData() {
            user.updateProfile({
                displayName: name.value
            }).then(function () {
                console.log('更名成功');
                //window.location.reload();
            }), function (error) {
                console.log('更名失敗:' + error);
                //window.location.reload();
            };
        });

        trophy.addEventListener('input', function writeData() {
            //write name into firebase's database
            firebase.database().ref('users/' + user.uid).update({
                trophy: trophy.value
            }).then(function () {
                console.log('獎盃數已登錄');
                //window.location.reload();
            });;
        });
        level.addEventListener('input', function writeData() {
            //write name into firebase's database
            firebase.database().ref('users/' + user.uid).update({
                level: level.value,
            }).then(function () {
                console.log('等級已登錄');
                //window.location.reload();
            });;
        });
    } else {
        name.style.display = "none";
        trophy.style.display = "none";
        level.style.display = "none";
    }
});
// let user can upload photo
var loadFile = function (event) {
    var reader = new FileReader();
    reader.onload = function () {
        var output = document.getElementById('output');
        output.src = reader.result;

    };
    reader.readAsDataURL(event.target.files[0]); // preview picture
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            // upload to storage
            var storageRef = firebase.storage().ref();
            var fileName = user.email + "photo.png";
            storageRef.child(fileName).put(event.target.files[0]).then(function (snapshot) {
                console.log('uploaded success');
            })
            //get download link
            storageRef.child(fileName).getDownloadURL().then(function (url) {
                // update user photo
                user.updateProfile({
                    photoURL: url
                }).then(function () {
                    //完成之後做的事
                    console.log('更新完成');
                    console.log(user.photoURL);
                    window.location.reload();
                }, function (error) {
                    console.log('錯誤: ' + error);
                });
            });
        } else {
            alert('please sign in !');
            window.location.href = "signin.html";
        }
    });
};


window.onload = function(){
    
    var b = document.getElementById('changepw');
    b.addEventListener('click',function () {
        var auth = firebase.auth();
        var emailAddress = auth.currentUser.email;
        auth.sendPasswordResetEmail(emailAddress).then(function () {
            // Email sent.
            console.log('email sent successful');
            alert('please go check out verification email')
        }).catch(function (error) {
            // An error happened.
            console.log('error: '+error);
        });
    });
};