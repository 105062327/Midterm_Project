function init() {
    var user_email = '';
    var user_name;
    var user_photo;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            user_name = user.displayName;
            user_photo = user.photoURL;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById("logout-btn");
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function () {
                    alert('success');
                    window.location.href = "signin.html"
                }).catch(
                    function () {
                        alert('sign out failed');
                    }
                )

            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
            alert("please sign in ");
            window.location.href = "signin.html"; 
        }
    });
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_title = document.getElementById('topic');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field

            var commentsRef = firebase.database().ref('com_list');
            var newPostRef = commentsRef.push();
            newPostRef.set({
                email: user_email,
                title: post_title.value,
                data: post_txt.value,
                displayName: user_name,
                photo: user_photo
            });
            post_txt.value = "";
            post_title.value = "";
        }
    });

    // The html code for post

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {


            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                var key = childSnapshot.key;
                //alert(key);
                var pre = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'>";
                var h_line = "";
                //alert(childData.photo);
                var PhotoPart = "<img src=' " + childData.photo + " ' alt='大頭貼' class=' col-bg-0.5' style='height:2rem; width:2rem;'>";
                var Paragraph = "<p class='media-body row pb-3 mb-0 small lh-125 border-bottom border-gray'>";
                var Owner = "<strong class='text-gray-dark col-sm-1 col-1 '>" + childData.displayName + "</strong>";
                var TitleAnchor = "<a class='col-sm-10 col-8' href='index.html?post=" + key + "' style='color:black;font-size:1.5rem;padding-left:3rem;'>" + childData.title + "</a>";
                var ending = "</p></div></div>\n";
                //<div class='my-3 p-3 bg-white rounded box-shadow'>
                //  <h6 class='border-bottom border-gray pb-2 mb-0' >recent post</h6>
                //  <div class='media text-muted pt-3'>
                //      <img src=' " + pic_src + " ' alt='' class='mr-2 rounded' style='height:32px; width:32px;'>
                //      <p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>
                //          <strong class='d-block text-gray-dark'>str_before_username + childData.displayName </strong>
                //          <a >Read more...</a>
                //
                //      </p>
                //  </div>
                //</div>
                total_post[total_post.length] = pre + h_line + Paragraph + TitleAnchor + PhotoPart + Owner + ending;
                first_count += 1;
            });
            document.getElementById("post_list").innerHTML = total_post.join('');

            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var key = data.key;
                    var pre = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'>";
                    var h_line = "";
                    var PhotoPart = "<img src=' " + childData.photo + " ' alt='大頭貼' class=' col-bg-0.5' style='height:2rem; width:2rem;'>";
                    var Paragraph = "<p class='media-body row pb-3 mb-0 small lh-125 border-bottom border-gray'>";
                    var Owner = "<strong class='text-gray-dark col-sm-1 col-1 '>" + childData.displayName + "</strong>";
                    var TitleAnchor = "<a class='col-sm-10 col-8' href='index.html?post=" + key + "' style='color:black;font-size:1.5rem;padding-left:3rem;'>" + childData.title + "</a>";
                    var ending = "</p></div></div>\n";
                    total_post[total_post.length] = pre + h_line + Paragraph + TitleAnchor + PhotoPart + Owner + ending;
                    document.getElementById("post_list").innerHTML = total_post.join('');
                }
            })
        })
        .catch(e => console.log(e.message));
}

function inspectUrl() {
    var cur = window.location.href;
    var res = cur.split("post=");
    if (res[1] !== undefined) {
        var PostKey = res[1];
        return PostKey;
    }
    return "";
}

window.onload = function () {
    var PostKey = inspectUrl();
    if (PostKey != "") {
        window.location.href = "post.html?" + PostKey;
    }
    
    init();
};