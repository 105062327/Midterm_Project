# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [野豬搖滾論壇]
* Key functions (add/delete)
    1. post list page (index.html)
    2. post page (post.html)
    3. user profile (profile.html)
    4. leave comment under any post
* Other functions (add/delete)
    1. User can upload his own profile image
    2. User can change his own password with email verification
    3. You can see how long the comment have been posted
    4. In every post, you can see people who post/comment with their photo 
    5. On the bottom of index page, you can thumbup my page with your Facebook account
    6. When someone comment any posts, you can get the notification from your chrome browser if you open the "google notification", and you can see which article he replied .

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
在一開始的登錄頁面中，中間淡入的圖片<img src="public/img/clashroyale_logo.jpg" width="200px">使用的方法是css的animation。
登入後，會轉到index.html，也就是這個論壇的post list page，在最下面有個fb的點讚功能，可以在fb上按讚本站。

post list page這裡可以看到很多文章，但看不到文章的內容，必須點擊文章標題才能跳轉至文章頁面(post page)。
在文章頁面中，有文章的內容，以及留言的人們，甚至可以看的到他們幾個小時(日)前留言的呢!

如果用chrome 開啟網站且接受來自google的通知(重要!)，如果有人在某篇文章下面留言，就可以收到來自瀏覽器的通知
![Alt text](public/img/notification.PNG)
點擊上方profile, 跳轉至profile.html，這裡可以自己上傳好看的大頭貼、修改暱稱、輸入一些遊戲的資訊、更換密碼...等等。
RWD 前
![Alt text](public/img/rwd1.PNG)
RWD 後
![Alt text](public/img/rwd2.PNG)
## Security Report (Optional)
1. 沒有註冊/登入就無法訪問，會被引導至登入頁面
2. 更改密碼要經過郵件認證
3. 沒有登入的情況寫無法存取資料庫(利用database 的rule設定)